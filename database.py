#!/usr/bin/python3

import requests

ISBN = input("Input Book ISBN:")

payload = {'q' : 'isbn:' + ISBN }

r = requests.get("https://www.googleapis.com/books/v1/volumes", params = payload)

print (r.text)

book = r.json()["items"][0]

title = book["volumeInfo"]["title"]
authors = book["volumeInfo"]["authors"]
printType = book ["volumeInfo"]["printType"]
pageCount = book["volumeInfo"]["pageCount"]
publishedDate = book["volumeInfo"]["publishedDate"]
webReaderLink = book["accessInfo"]["webReaderLink"]

print(book)
