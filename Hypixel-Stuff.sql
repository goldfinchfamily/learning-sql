/*Player Table. Has all important data*/
CREATE TABLE player (
    id INTEGER PRIMARY KEY,
    Username TEXT,
    Health INTEGER, 
    Defense INTEGER, 
    Strength INTEGER, 
    Speed INTEGER, 
    "Crit Chance" INTEGER, 
    "Crit Damage" INTEGER,
    Intelligence INTEGER,
    "Fairy Souls" INTEGER, 
    "Mining Level" INTEGER,
    "Foraging Level" INTEGER,
    "Enchanting Level" INTEGER, 
    "Carpentry Level" INTEGER,
    "Farming Level" INTEGER, 
    "Combat Level" INTEGER,
    "Fishing Level" INTEGER,
    "Alchemy Level" INTEGER,
    "Runecrafting Level" INTEGER);

/*Player Data*/
INSERT INTO player VALUES (1,"Ninjagoldfinch",310,48,46,103,39,54,26,114,19,18,12,0,16,19,10,14,14);
INSERT INTO player VALUES (2,"Angryteddy69",387,55,45,104,37,52,50,130,19,14,15,0,13,17,9,8,11);


/*Show player data*/
SELECT * FROM player;

CREATE TABLE items (
  id INTEGER PRIMARY KEY,
  "Name" TEXT,
  Rarity INTEGER DEFAULT 5,
  "Type" TEXT,
  Health INTEGER DEFAULT 0,
  Defense INTEGER DEFAULT 0, 
  Damage INTEGER NOT NULL DEFAULT 0, 
  Strength INTEGER NOT NULL DEFAULT 0, 
  Speed INTEGER NOT NULL DEFAULT 0, 
  "Crit Damage" INTEGER NOT NULL DEFAULT 0, 
  "Crit Chance" INTEGER NOT NULL DEFAULT 0, 
  Intelligence INTEGER NOT NULL DEFAULT 0, 
  Mana INTEGER NOT NULL DEFAULT 0,
  "Sea Creature Chance" INTEGER NOT NULL DEFAULT 0,
  "Attack speed" INTEGER NOT NULL DEFAULT 0,
  "Full set" TEXT NOT NULL DEFAULT "N/A" 
);  
  
CREATE TABLE rarity (
    Value INTEGER NOT NULL,
    Label TEXT NOT NULL
    );
    
    
CREATE TABLE type (
    Value INTEGER NOT NULL,
    Label TEXT NOT NULL
    );
    
    
INSERT INTO rarity VALUES (1,"Legendary");
INSERT INTO rarity VALUES (2,"Epic");
INSERT INTO rarity VALUES (3,"Rare");
INSERT INTO rarity VALUES (4,"Uncommon");
INSERT INTO rarity VALUES (5,"Common");
INSERT INTO rarity VALUES (6,"Special");

INSERT INTO type VALUES (1, "Sword");
INSERT INTO type VALUES (2, "Full Armor Set");
INSERT INTO type VALUES (3, "Armor Piece");


INSERT INTO items (id, Name, Rarity, Type, Damage) VALUES (1,"Aspect of the Jerry",5,1,1);
INSERT INTO items (id, Name, Rarity, Type, Damage) VALUES (2,"Fancy Sword",5,1,20);
INSERT INTO items (id, Name, Rarity, Type, Damage, Mana) VALUES (3,"Rogue Sword",5,1,20,50);
INSERT INTO items (id, Name, Rarity, Type, Damage) VALUES (4,"Spider Sword",5,1,30);
INSERT INTO items (id, Name, Rarity, Type, Damage) VALUES (5,"Undead Sword",5,1,30);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength) VALUES (6,"Thick Aspect of the Jerry",4,1,1,100);
INSERT INTO items (id, Name, Rarity, Type, Damage) VALUES (7,"End Sword",4,1,35);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength) VALUES (8,"Cleaver",4,1,40,10);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength) VALUES (9,"Flaming Sword",4,1,50,20);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength) VALUES (10,"Prismarine Blade",4,1,50,20);
INSERT INTO items (id, Name, Rarity, Type, Damage, Speed) VALUES (11,"Hunter Knife",4,1,50,40);
INSERT INTO items (id, Name, Rarity, Type, Damage) VALUES (12,"Silver Fang",4,1,100);
INSERT INTO items (id, Name, Rarity, Type, Damage, Mana) VALUES (13,"Frozen Scythe",3,1,80,50);
INSERT INTO items (id, Name, Rarity, Type, Damage, "Crit Chance") VALUES (14,"Tactician's Sword",3,1,50,20);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength) VALUES (15,"Raider Axe",3,1,80,50);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength, Defense, Mana) VALUES (16,"Golem Sword",3,1,80,125,25,70);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength, Intelligence) VALUES(17,"Revenant Falchion",3,1,90,50,100);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength,Mana) VALUES (18,"Aspect of the End",3,1,100,100,50);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength, "Crit Damage", Intelligence, Mana) VALUES (19,"Zombie Sword",3,1,100,30,20,50,70);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength, "Crit Damage") VALUES (20,"Recluse Fang",3,1,120,30,20);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength, Mana) VALUES (21,"Edible Mace", 3,1, 125,25,100);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength, Speed) VALUES (22,"Shaman Sword",2,1,100,20,5);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength, "Crit Damage") VALUES (23,"Thick Tactician's Sword",2,1,50,100,20);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength, Intelligence, Mana) VALUES (24,"Ember Rod",2,1,80,35,50,150);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength,Intelligence, Mana) VALUES (25,"Ornate Zombie Sword",2,1,110,60,50,70);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength) VALUES (26,"Scopion Foil",2,1,100,100);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength) VALUES (27,"End Stone Sword",2,1,120,80);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength, Intelligence) VALUES (28, "Reaper Falchion",2,1,120,100,200);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength) VALUES (29,"Ink Wand",2,1,130,90);
INSERT INTO items (id, Name, Rarity, Type, Damage) VALUES (30,"Emerald Blade",2,1,130);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength, "Crit Damage", Mana) VALUES (31,"Leaping Sword",2,1,150,100,25,50);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength, "Crit Damage", Mana) VALUES (32,"Sik Edge Sword",2,1,175,125,25,50);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength, Speed) VALUES (33,"Pooch Sword",1,1,120,20,5);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength) VALUES (34,"Thick Scopion Foil",1,1,100,200);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength, Intelligence) VALUES (35,"Yeti Sword",1,1,150,150,50);
INSERT INTO items (id, Name, Rarity, Type, Damage) VALUES (36,"Midas Sword ($100k)",1,1,150);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength, "Crit Chance", "Crit Damage") VALUES (37,"Pigman Sword",1,1,200,100,5,30);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength) VALUES (38,"Aspect of the dragons",1,1,225,100);
INSERT INTO items (id, Name, Rarity, Type, Damage, Strength) VALUES (39, "Midas Sword (50 Mil)",1,1,270,120);
INSERT INTO items (id, Name, Rarity, Type, Damage, Speed) VALUES (40,"Reaper Scythe",1,1,333,10);

/*Show Weapons*/
SELECT
  Name,
  r.label AS Rarity,
  t.label,
  Damage,
  Strength,
  Speed,
  "Crit Damage",
  "Crit Chance",
  Mana,
  Intelligence
 FROM
  items AS i,
  rarity AS r,
  type as t
 WHERE
  i.Rarity = r.value AND
  i.Type = t.value AND
  i.Type = 1
 ORDER BY
 i.Rarity,Damage DESC, Strength DESC;

/*Show Full Set's Of Armor*/
SELECT
  Name,
  r.label AS Rarity,
  t.label AS Type,
  Health,
  Defense,
  Strength
 FROM
  items AS i,
  rarity AS r,
  type as t
 WHERE
  i.Rarity = r.value AND
  i.Type = t.value AND
  i.Type = 2
 ORDER BY
 i.Rarity, Health, Defense;
 
 
 /*Show Full Set's Of Armor*/
SELECT
  Name,
  r.label AS Rarity,
  t.label AS Type,
  Health,
  Defense,
  Strength,
  "Full Set"
 FROM
  items AS i,
  rarity AS r,
  type as t
 WHERE
  i.Rarity = r.value AND
  i.Type = t.value AND
  i.Type = 2
 ORDER BY
 i.Rarity, Health, Defense;


 
 
 
